﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.txtSTODprocess = New System.Windows.Forms.TextBox()
        Me.txtCleaningProcess = New System.Windows.Forms.TextBox()
        Me.lblCustomerName = New System.Windows.Forms.Label()
        Me.txtCopyFileToStaging = New System.Windows.Forms.TextBox()
        Me.txtImportData = New System.Windows.Forms.TextBox()
        Me.pnlLoadStatus = New System.Windows.Forms.Panel()
        Me.txtLoadStatus_MPAN = New System.Windows.Forms.TextBox()
        Me.txtLoadStatus_CheckDays = New System.Windows.Forms.TextBox()
        Me.txtLoadStatus_TenderLoaded = New System.Windows.Forms.TextBox()
        Me.txtLoadStatus_Path = New System.Windows.Forms.TextBox()
        Me.btnExitApplication = New System.Windows.Forms.Button()
        Me.lstMPANs = New System.Windows.Forms.ListBox()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.pnlSelected = New System.Windows.Forms.Panel()
        Me.btnCheckHHDataFile = New System.Windows.Forms.Button()
        Me.btnSelectHHFile = New System.Windows.Forms.Button()
        Me.txtFileName_Selected = New System.Windows.Forms.TextBox()
        Me.lblSelected = New System.Windows.Forms.Label()
        Me.txtCustomerName_Selected = New System.Windows.Forms.TextBox()
        Me.txtSiteName_Selected = New System.Windows.Forms.TextBox()
        Me.txtTenderName_Selected = New System.Windows.Forms.TextBox()
        Me.lblFilter = New System.Windows.Forms.Label()
        Me.btnSetVariables = New System.Windows.Forms.Button()
        Me.txtCustomerName = New System.Windows.Forms.TextBox()
        Me.lstCustomerName = New System.Windows.Forms.ListBox()
        Me.lblTenderName = New System.Windows.Forms.Label()
        Me.lstSiteName = New System.Windows.Forms.ListBox()
        Me.txtSiteName = New System.Windows.Forms.TextBox()
        Me.lblSiteName = New System.Windows.Forms.Label()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.txtTenderName = New System.Windows.Forms.TextBox()
        Me.lstTenderName = New System.Windows.Forms.ListBox()
        Me.pnlFilter = New System.Windows.Forms.Panel()
        Me.pnlLoadStatus.SuspendLayout()
        Me.pnlSelected.SuspendLayout()
        Me.pnlFilter.SuspendLayout()
        Me.SuspendLayout()
        '
        'txtSTODprocess
        '
        Me.txtSTODprocess.Enabled = False
        Me.txtSTODprocess.Location = New System.Drawing.Point(14, 160)
        Me.txtSTODprocess.Name = "txtSTODprocess"
        Me.txtSTODprocess.Size = New System.Drawing.Size(598, 20)
        Me.txtSTODprocess.TabIndex = 7
        Me.txtSTODprocess.Text = "STOD band processing"
        Me.txtSTODprocess.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtCleaningProcess
        '
        Me.txtCleaningProcess.Enabled = False
        Me.txtCleaningProcess.Location = New System.Drawing.Point(14, 185)
        Me.txtCleaningProcess.Name = "txtCleaningProcess"
        Me.txtCleaningProcess.Size = New System.Drawing.Size(598, 20)
        Me.txtCleaningProcess.TabIndex = 6
        Me.txtCleaningProcess.Text = "cleaning process"
        Me.txtCleaningProcess.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lblCustomerName
        '
        Me.lblCustomerName.AutoSize = True
        Me.lblCustomerName.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.lblCustomerName.Location = New System.Drawing.Point(85, 9)
        Me.lblCustomerName.Name = "lblCustomerName"
        Me.lblCustomerName.Size = New System.Drawing.Size(82, 13)
        Me.lblCustomerName.TabIndex = 19
        Me.lblCustomerName.Text = "Customer Name"
        '
        'txtCopyFileToStaging
        '
        Me.txtCopyFileToStaging.Enabled = False
        Me.txtCopyFileToStaging.Location = New System.Drawing.Point(14, 60)
        Me.txtCopyFileToStaging.Name = "txtCopyFileToStaging"
        Me.txtCopyFileToStaging.Size = New System.Drawing.Size(598, 20)
        Me.txtCopyFileToStaging.TabIndex = 5
        Me.txtCopyFileToStaging.Text = "copy file to staging area"
        Me.txtCopyFileToStaging.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtImportData
        '
        Me.txtImportData.Enabled = False
        Me.txtImportData.Location = New System.Drawing.Point(14, 135)
        Me.txtImportData.Name = "txtImportData"
        Me.txtImportData.Size = New System.Drawing.Size(598, 20)
        Me.txtImportData.TabIndex = 4
        Me.txtImportData.Text = "data imported"
        Me.txtImportData.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'pnlLoadStatus
        '
        Me.pnlLoadStatus.BackColor = System.Drawing.Color.LightSteelBlue
        Me.pnlLoadStatus.Controls.Add(Me.txtSTODprocess)
        Me.pnlLoadStatus.Controls.Add(Me.txtCleaningProcess)
        Me.pnlLoadStatus.Controls.Add(Me.txtCopyFileToStaging)
        Me.pnlLoadStatus.Controls.Add(Me.txtImportData)
        Me.pnlLoadStatus.Controls.Add(Me.txtLoadStatus_MPAN)
        Me.pnlLoadStatus.Controls.Add(Me.txtLoadStatus_CheckDays)
        Me.pnlLoadStatus.Controls.Add(Me.txtLoadStatus_TenderLoaded)
        Me.pnlLoadStatus.Controls.Add(Me.txtLoadStatus_Path)
        Me.pnlLoadStatus.Location = New System.Drawing.Point(81, 104)
        Me.pnlLoadStatus.Name = "pnlLoadStatus"
        Me.pnlLoadStatus.Size = New System.Drawing.Size(626, 216)
        Me.pnlLoadStatus.TabIndex = 20
        '
        'txtLoadStatus_MPAN
        '
        Me.txtLoadStatus_MPAN.Enabled = False
        Me.txtLoadStatus_MPAN.Location = New System.Drawing.Point(14, 110)
        Me.txtLoadStatus_MPAN.Name = "txtLoadStatus_MPAN"
        Me.txtLoadStatus_MPAN.Size = New System.Drawing.Size(598, 20)
        Me.txtLoadStatus_MPAN.TabIndex = 3
        Me.txtLoadStatus_MPAN.Text = "does the MPAN match with the file"
        Me.txtLoadStatus_MPAN.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtLoadStatus_CheckDays
        '
        Me.txtLoadStatus_CheckDays.Enabled = False
        Me.txtLoadStatus_CheckDays.Location = New System.Drawing.Point(14, 85)
        Me.txtLoadStatus_CheckDays.Name = "txtLoadStatus_CheckDays"
        Me.txtLoadStatus_CheckDays.Size = New System.Drawing.Size(598, 20)
        Me.txtLoadStatus_CheckDays.TabIndex = 2
        Me.txtLoadStatus_CheckDays.Text = "are there the correct number of days data"
        Me.txtLoadStatus_CheckDays.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtLoadStatus_TenderLoaded
        '
        Me.txtLoadStatus_TenderLoaded.Enabled = False
        Me.txtLoadStatus_TenderLoaded.Location = New System.Drawing.Point(14, 35)
        Me.txtLoadStatus_TenderLoaded.Name = "txtLoadStatus_TenderLoaded"
        Me.txtLoadStatus_TenderLoaded.Size = New System.Drawing.Size(598, 20)
        Me.txtLoadStatus_TenderLoaded.TabIndex = 1
        Me.txtLoadStatus_TenderLoaded.Text = "has the tender already been loaded"
        Me.txtLoadStatus_TenderLoaded.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtLoadStatus_Path
        '
        Me.txtLoadStatus_Path.Enabled = False
        Me.txtLoadStatus_Path.Location = New System.Drawing.Point(14, 10)
        Me.txtLoadStatus_Path.Name = "txtLoadStatus_Path"
        Me.txtLoadStatus_Path.Size = New System.Drawing.Size(598, 20)
        Me.txtLoadStatus_Path.TabIndex = 0
        Me.txtLoadStatus_Path.Text = "is the tender file path ok"
        Me.txtLoadStatus_Path.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'btnExitApplication
        '
        Me.btnExitApplication.BackColor = System.Drawing.Color.Red
        Me.btnExitApplication.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.btnExitApplication.ForeColor = System.Drawing.Color.White
        Me.btnExitApplication.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnExitApplication.Location = New System.Drawing.Point(8, 389)
        Me.btnExitApplication.Name = "btnExitApplication"
        Me.btnExitApplication.Size = New System.Drawing.Size(808, 38)
        Me.btnExitApplication.TabIndex = 24
        Me.btnExitApplication.Text = "Exit Application"
        Me.btnExitApplication.UseVisualStyleBackColor = False
        '
        'lstMPANs
        '
        Me.lstMPANs.FormattingEnabled = True
        Me.lstMPANs.Location = New System.Drawing.Point(822, 27)
        Me.lstMPANs.Name = "lstMPANs"
        Me.lstMPANs.Size = New System.Drawing.Size(120, 95)
        Me.lstMPANs.TabIndex = 23
        Me.lstMPANs.Visible = False
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        '
        'pnlSelected
        '
        Me.pnlSelected.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.pnlSelected.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlSelected.Controls.Add(Me.pnlLoadStatus)
        Me.pnlSelected.Controls.Add(Me.btnCheckHHDataFile)
        Me.pnlSelected.Controls.Add(Me.btnSelectHHFile)
        Me.pnlSelected.Controls.Add(Me.txtFileName_Selected)
        Me.pnlSelected.Controls.Add(Me.lblSelected)
        Me.pnlSelected.Controls.Add(Me.txtCustomerName_Selected)
        Me.pnlSelected.Controls.Add(Me.txtSiteName_Selected)
        Me.pnlSelected.Controls.Add(Me.txtTenderName_Selected)
        Me.pnlSelected.Location = New System.Drawing.Point(7, 27)
        Me.pnlSelected.Name = "pnlSelected"
        Me.pnlSelected.Size = New System.Drawing.Size(729, 356)
        Me.pnlSelected.TabIndex = 22
        Me.pnlSelected.Visible = False
        '
        'btnCheckHHDataFile
        '
        Me.btnCheckHHDataFile.Enabled = False
        Me.btnCheckHHDataFile.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnCheckHHDataFile.Location = New System.Drawing.Point(81, 66)
        Me.btnCheckHHDataFile.Name = "btnCheckHHDataFile"
        Me.btnCheckHHDataFile.Size = New System.Drawing.Size(626, 23)
        Me.btnCheckHHDataFile.TabIndex = 19
        Me.btnCheckHHDataFile.Text = "Check HH Data File"
        Me.btnCheckHHDataFile.UseVisualStyleBackColor = True
        '
        'btnSelectHHFile
        '
        Me.btnSelectHHFile.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnSelectHHFile.Location = New System.Drawing.Point(622, 38)
        Me.btnSelectHHFile.Name = "btnSelectHHFile"
        Me.btnSelectHHFile.Size = New System.Drawing.Size(86, 23)
        Me.btnSelectHHFile.TabIndex = 18
        Me.btnSelectHHFile.Text = "Select File"
        Me.btnSelectHHFile.UseVisualStyleBackColor = True
        '
        'txtFileName_Selected
        '
        Me.txtFileName_Selected.Location = New System.Drawing.Point(81, 40)
        Me.txtFileName_Selected.Name = "txtFileName_Selected"
        Me.txtFileName_Selected.ReadOnly = True
        Me.txtFileName_Selected.Size = New System.Drawing.Size(535, 20)
        Me.txtFileName_Selected.TabIndex = 17
        '
        'lblSelected
        '
        Me.lblSelected.AutoSize = True
        Me.lblSelected.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.lblSelected.ForeColor = System.Drawing.Color.Black
        Me.lblSelected.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.lblSelected.Location = New System.Drawing.Point(19, 15)
        Me.lblSelected.Name = "lblSelected"
        Me.lblSelected.Size = New System.Drawing.Size(49, 13)
        Me.lblSelected.TabIndex = 16
        Me.lblSelected.Text = "Selected"
        '
        'txtCustomerName_Selected
        '
        Me.txtCustomerName_Selected.Location = New System.Drawing.Point(81, 12)
        Me.txtCustomerName_Selected.Name = "txtCustomerName_Selected"
        Me.txtCustomerName_Selected.Size = New System.Drawing.Size(205, 20)
        Me.txtCustomerName_Selected.TabIndex = 7
        '
        'txtSiteName_Selected
        '
        Me.txtSiteName_Selected.Location = New System.Drawing.Point(292, 12)
        Me.txtSiteName_Selected.Name = "txtSiteName_Selected"
        Me.txtSiteName_Selected.Size = New System.Drawing.Size(205, 20)
        Me.txtSiteName_Selected.TabIndex = 8
        '
        'txtTenderName_Selected
        '
        Me.txtTenderName_Selected.Location = New System.Drawing.Point(503, 12)
        Me.txtTenderName_Selected.Name = "txtTenderName_Selected"
        Me.txtTenderName_Selected.Size = New System.Drawing.Size(205, 20)
        Me.txtTenderName_Selected.TabIndex = 12
        '
        'lblFilter
        '
        Me.lblFilter.AutoSize = True
        Me.lblFilter.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.lblFilter.Location = New System.Drawing.Point(19, 16)
        Me.lblFilter.Name = "lblFilter"
        Me.lblFilter.Size = New System.Drawing.Size(29, 13)
        Me.lblFilter.TabIndex = 17
        Me.lblFilter.Text = "Filter"
        '
        'btnSetVariables
        '
        Me.btnSetVariables.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnSetVariables.Location = New System.Drawing.Point(743, 27)
        Me.btnSetVariables.Name = "btnSetVariables"
        Me.btnSetVariables.Size = New System.Drawing.Size(73, 356)
        Me.btnSetVariables.TabIndex = 17
        Me.btnSetVariables.Text = "Select"
        Me.btnSetVariables.UseVisualStyleBackColor = True
        '
        'txtCustomerName
        '
        Me.txtCustomerName.Location = New System.Drawing.Point(81, 13)
        Me.txtCustomerName.Name = "txtCustomerName"
        Me.txtCustomerName.Size = New System.Drawing.Size(205, 20)
        Me.txtCustomerName.TabIndex = 0
        '
        'lstCustomerName
        '
        Me.lstCustomerName.FormattingEnabled = True
        Me.lstCustomerName.Location = New System.Drawing.Point(81, 48)
        Me.lstCustomerName.Name = "lstCustomerName"
        Me.lstCustomerName.Size = New System.Drawing.Size(205, 290)
        Me.lstCustomerName.TabIndex = 4
        '
        'lblTenderName
        '
        Me.lblTenderName.AutoSize = True
        Me.lblTenderName.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.lblTenderName.Location = New System.Drawing.Point(507, 9)
        Me.lblTenderName.Name = "lblTenderName"
        Me.lblTenderName.Size = New System.Drawing.Size(41, 13)
        Me.lblTenderName.TabIndex = 21
        Me.lblTenderName.Text = "Tender"
        '
        'lstSiteName
        '
        Me.lstSiteName.FormattingEnabled = True
        Me.lstSiteName.Location = New System.Drawing.Point(292, 48)
        Me.lstSiteName.Name = "lstSiteName"
        Me.lstSiteName.Size = New System.Drawing.Size(205, 290)
        Me.lstSiteName.TabIndex = 5
        '
        'txtSiteName
        '
        Me.txtSiteName.Location = New System.Drawing.Point(292, 13)
        Me.txtSiteName.Name = "txtSiteName"
        Me.txtSiteName.Size = New System.Drawing.Size(205, 20)
        Me.txtSiteName.TabIndex = 1
        '
        'lblSiteName
        '
        Me.lblSiteName.AutoSize = True
        Me.lblSiteName.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.lblSiteName.Location = New System.Drawing.Point(296, 9)
        Me.lblSiteName.Name = "lblSiteName"
        Me.lblSiteName.Size = New System.Drawing.Size(56, 13)
        Me.lblSiteName.TabIndex = 20
        Me.lblSiteName.Text = "Site Name"
        '
        'Timer1
        '
        Me.Timer1.Enabled = True
        '
        'txtTenderName
        '
        Me.txtTenderName.Location = New System.Drawing.Point(502, 13)
        Me.txtTenderName.Name = "txtTenderName"
        Me.txtTenderName.Size = New System.Drawing.Size(205, 20)
        Me.txtTenderName.TabIndex = 2
        '
        'lstTenderName
        '
        Me.lstTenderName.FormattingEnabled = True
        Me.lstTenderName.Location = New System.Drawing.Point(502, 48)
        Me.lstTenderName.Name = "lstTenderName"
        Me.lstTenderName.Size = New System.Drawing.Size(205, 290)
        Me.lstTenderName.TabIndex = 6
        '
        'pnlFilter
        '
        Me.pnlFilter.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.pnlFilter.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlFilter.Controls.Add(Me.lstCustomerName)
        Me.pnlFilter.Controls.Add(Me.lstSiteName)
        Me.pnlFilter.Controls.Add(Me.lblFilter)
        Me.pnlFilter.Controls.Add(Me.txtCustomerName)
        Me.pnlFilter.Controls.Add(Me.lstTenderName)
        Me.pnlFilter.Controls.Add(Me.txtSiteName)
        Me.pnlFilter.Controls.Add(Me.txtTenderName)
        Me.pnlFilter.Location = New System.Drawing.Point(7, 27)
        Me.pnlFilter.Name = "pnlFilter"
        Me.pnlFilter.Size = New System.Drawing.Size(729, 356)
        Me.pnlFilter.TabIndex = 18
        Me.pnlFilter.Tag = ""
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(819, 430)
        Me.Controls.Add(Me.lblCustomerName)
        Me.Controls.Add(Me.btnExitApplication)
        Me.Controls.Add(Me.lstMPANs)
        Me.Controls.Add(Me.pnlSelected)
        Me.Controls.Add(Me.btnSetVariables)
        Me.Controls.Add(Me.lblTenderName)
        Me.Controls.Add(Me.lblSiteName)
        Me.Controls.Add(Me.pnlFilter)
        Me.Name = "frmMain"
        Me.Text = "Form1"
        Me.pnlLoadStatus.ResumeLayout(False)
        Me.pnlLoadStatus.PerformLayout()
        Me.pnlSelected.ResumeLayout(False)
        Me.pnlSelected.PerformLayout()
        Me.pnlFilter.ResumeLayout(False)
        Me.pnlFilter.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtSTODprocess As System.Windows.Forms.TextBox
    Friend WithEvents txtCleaningProcess As System.Windows.Forms.TextBox
    Friend WithEvents lblCustomerName As System.Windows.Forms.Label
    Friend WithEvents txtCopyFileToStaging As System.Windows.Forms.TextBox
    Friend WithEvents txtImportData As System.Windows.Forms.TextBox
    Friend WithEvents pnlLoadStatus As System.Windows.Forms.Panel
    Friend WithEvents txtLoadStatus_MPAN As System.Windows.Forms.TextBox
    Friend WithEvents txtLoadStatus_CheckDays As System.Windows.Forms.TextBox
    Friend WithEvents txtLoadStatus_TenderLoaded As System.Windows.Forms.TextBox
    Friend WithEvents txtLoadStatus_Path As System.Windows.Forms.TextBox
    Friend WithEvents btnExitApplication As System.Windows.Forms.Button
    Friend WithEvents lstMPANs As System.Windows.Forms.ListBox
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents pnlSelected As System.Windows.Forms.Panel
    Friend WithEvents btnCheckHHDataFile As System.Windows.Forms.Button
    Friend WithEvents btnSelectHHFile As System.Windows.Forms.Button
    Friend WithEvents txtFileName_Selected As System.Windows.Forms.TextBox
    Friend WithEvents lblSelected As System.Windows.Forms.Label
    Friend WithEvents txtCustomerName_Selected As System.Windows.Forms.TextBox
    Friend WithEvents txtSiteName_Selected As System.Windows.Forms.TextBox
    Friend WithEvents txtTenderName_Selected As System.Windows.Forms.TextBox
    Friend WithEvents lblFilter As System.Windows.Forms.Label
    Friend WithEvents btnSetVariables As System.Windows.Forms.Button
    Friend WithEvents txtCustomerName As System.Windows.Forms.TextBox
    Friend WithEvents lstCustomerName As System.Windows.Forms.ListBox
    Friend WithEvents lblTenderName As System.Windows.Forms.Label
    Friend WithEvents lstSiteName As System.Windows.Forms.ListBox
    Friend WithEvents txtSiteName As System.Windows.Forms.TextBox
    Friend WithEvents lblSiteName As System.Windows.Forms.Label
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents txtTenderName As System.Windows.Forms.TextBox
    Friend WithEvents lstTenderName As System.Windows.Forms.ListBox
    Friend WithEvents pnlFilter As System.Windows.Forms.Panel

End Class
