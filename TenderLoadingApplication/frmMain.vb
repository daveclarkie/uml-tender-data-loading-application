﻿Imports TenderLoadingApplication.Core
Imports System.Windows.Forms.Form

Public Class frmMain




    Private Sub frmMain_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try

            LoadCustomers()
            Me.txtCustomerName.Focus()
        Catch ex As Exception
            Print(ex.Message)
        End Try
    End Sub

    Private Sub LoadCustomers()

        Dim strCustomerSQL As String = "SELECT custid, customername FROM UML_CMS.dbo.tblCustomer "
        strCustomerSQL += " WHERE (CustomerName like '" & Me.txtCustomerName.Text.ToString & "%'"
        strCustomerSQL += " OR CustomerName like '%" & Me.txtCustomerName.Text.ToString & "%'"
        strCustomerSQL += " OR CustomerName like '%" & Me.txtCustomerName.Text.ToString & "'"
        strCustomerSQL += " )"
        strCustomerSQL += " ORDER BY CustomerName ASC"


        With Me.lstCustomerName
            .DataSource = Core.data_select(strCustomerSQL)
            .ValueMember = "Custid"
            .DisplayMember = "customername"
        End With


    End Sub
    Private Sub LoadSites(ByVal CustID As Integer)

        Dim strSiteSQL As String = ""
        If Me.lstCustomerName.Items.Count > 0 Then

            strSiteSQL = "SELECT siteid, SiteName FROM UML_CMS.dbo.tblSites WHERE "
            strSiteSQL += " (SiteName like '" & Me.txtSiteName.Text.ToString & "%'"
            strSiteSQL += " OR SiteName like '%" & Me.txtSiteName.Text.ToString & "%'"
            strSiteSQL += " OR SiteName like '%" & Me.txtSiteName.Text.ToString & "'"
            strSiteSQL += " )"
            strSiteSQL += " AND custid = " & CustID
            strSiteSQL += " ORDER BY SiteName ASC"

        Else

            strSiteSQL = "SELECT 0 as siteid, 'no customer selected' as sitename "

        End If

        With Me.lstSiteName
            .DataSource = Core.data_select(strSiteSQL)
            .ValueMember = "siteid"
            .DisplayMember = "SiteName"
        End With

    End Sub
    Private Sub LoadTenders(ByVal SiteID As Integer)

        Dim strTenderSQL As String = ""
        If SiteID > 0 Then

            strTenderSQL = " SELECT tenderid AS TenderID, tenderdesc + ' (' + CAST(tenderid as varchar(100)) + ')' AS TenderDescription  "
            strTenderSQL += " FROM UML_CMS.dbo.tbltenders WHERE "
            strTenderSQL += " (tenderdesc + ' (' + CAST(tenderid as varchar(100)) + ')' like '" & Me.txtTenderName.Text.ToString & "%'"
            strTenderSQL += " OR tenderdesc + ' (' + CAST(tenderid as varchar(100)) + ')' like '%" & Me.txtTenderName.Text.ToString & "%'"
            strTenderSQL += " OR tenderdesc + ' (' + CAST(tenderid as varchar(100)) + ')' like '%" & Me.txtTenderName.Text.ToString & "'"
            strTenderSQL += " )"
            strTenderSQL += " AND siteid = " & SiteID
            strTenderSQL += " ORDER BY tenderdesc + ' (' + CAST(tenderid as varchar(100)) + ')' ASC"

        Else

            strTenderSQL = "SELECT 0 as tenderid, 'no site selected' as TenderDescription "

        End If

        With Me.lstTenderName
            .DataSource = Core.data_select(strTenderSQL)
            .ValueMember = "tenderid"
            .DisplayMember = "TenderDescription"
        End With

    End Sub

    Private Sub lstCustomerName_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstCustomerName.SelectedValueChanged
        If Me.lstCustomerName.Items.Count > 0 Then
            LoadSites(Me.lstCustomerName.SelectedItem.row.item(0))
            Me.txtCustomerName_Selected.Text = Core.data_select_value("SELECT CustomerName FROM UML_CMS.dbo.tblCustomer WHERE custid = " & Me.lstCustomerName.SelectedItem.row.item(0))
        Else
            LoadSites(0)
            Me.txtCustomerName_Selected.Text = Nothing
        End If

    End Sub
    Private Sub txtCustomerName_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtCustomerName.TextChanged
        LoadCustomers()
    End Sub

    Private Sub lstSiteName_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstSiteName.SelectedValueChanged
        LoadTenders(Me.lstSiteName.SelectedItem.row.item(0))
        Me.txtSiteName_Selected.Text = Core.data_select_value("SELECT SiteName FROM UML_CMS.dbo.tblSites WHERE siteid = " & Me.lstSiteName.SelectedItem.row.item(0))

        Dim strMPANSQL As String = "SELECT distribution + supplier as varMPAN FROM UML_CMS.dbo.tblSiteMpans WHERE FKSite_ID = " & Me.lstSiteName.SelectedItem.row.item(0)

        With Me.lstMPANs
            .DataSource = Core.data_select(strMPANSQL)
            .ValueMember = "varMPAN"
            .DisplayMember = "varMPAN"
        End With
    End Sub
    Private Sub txtSiteName_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSiteName.TextChanged
        LoadSites(Me.lstCustomerName.SelectedItem.row.item(0))
    End Sub

    Private Sub lstTenderName_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstTenderName.SelectedValueChanged
        Me.txtTenderName_Selected.Text = Core.data_select_value("SELECT CASE WHEN pricetype IS NULL THEN tenderdesc + ' | ' ELSE CASE WHEN tenderdesc IS NULL THEN  ' | ' + pricetype ELSE tenderdesc + ' | ' + pricetype  END END AS TenderDescription FROM UML_CMS.dbo.tblTenders WHERE tenderid = " & Me.lstTenderName.SelectedItem.row.item(0))
    End Sub
    Private Sub txtTenderName_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtTenderName.TextChanged
        LoadTenders(Me.lstSiteName.SelectedItem.row.item(0))
    End Sub

    Private Sub btnSetVariables_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSetVariables.Click

        If Me.btnSetVariables.Text = "Select" Then
            Me.pnlSelected.Visible = True
            Me.pnlFilter.Visible = False

            Me.btnSetVariables.Text = "De-select"
            Me.btnSetVariables.Size = New Size(73, 356)
        Else
            Me.pnlSelected.Visible = False
            Me.pnlFilter.Visible = True
            Me.txtFileName_Selected.Text = Nothing

            Me.btnSetVariables.Text = "Select"
            Me.btnSetVariables.Size = New Size(73, 356)

        End If

        ClearFileChecks()
    End Sub

    Private Sub txtFileName_Selected_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtFileName_Selected.TextChanged
        If Me.txtFileName_Selected.TextLength > 0 Then
            Me.btnCheckHHDataFile.Enabled = True
        Else
            Me.btnCheckHHDataFile.Enabled = False
        End If
    End Sub
    Private Sub btnSelectHHFile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSelectHHFile.Click

        ClearFileChecks()

        OpenFileDialog1.Title = "Please Select the HH Data file"
        OpenFileDialog1.InitialDirectory = "\\mceg.local\customers\"
        OpenFileDialog1.Filter = "Text files (*.txt)|*.txt|CSV Files (*.csv)|*.csv"
        OpenFileDialog1.FileName = "select a file..."

        OpenFileDialog1.ShowDialog()

    End Sub
    Private Sub OpenFileDialog1_FileOk(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles OpenFileDialog1.FileOk
        Dim strm As System.IO.Stream
        strm = OpenFileDialog1.OpenFile()
        txtFileName_Selected.Text = OpenFileDialog1.FileName.ToString()
        If Not (strm Is Nothing) Then
            'insert code to read the file data
            strm.Close()
            'MessageBox.Show("file closed")
        End If
    End Sub


    Private Sub ClearFileChecks()
        Me.txtLoadStatus_Path.BackColor = Nothing
        Me.txtLoadStatus_Path.Text = "is the tender file path ok"

        Me.txtLoadStatus_TenderLoaded.BackColor = Nothing
        Me.txtLoadStatus_TenderLoaded.Text = "has the tender already been loaded"

        Me.txtCopyFileToStaging.BackColor = Nothing
        Me.txtCopyFileToStaging.Text = "copy file to staging area"

        Me.txtLoadStatus_CheckDays.BackColor = Nothing
        Me.txtLoadStatus_CheckDays.Text = "are there the correct number of days data"

        Me.txtLoadStatus_MPAN.BackColor = Nothing
        Me.txtLoadStatus_MPAN.Text = "does the MPAN match with the file"

        Me.txtCleaningProcess.BackColor = Nothing
        Me.txtCleaningProcess.Text = "cleaning process"

        Me.txtImportData.BackColor = Nothing
        Me.txtImportData.Text = "data imported"

        Me.txtSTODprocess.backcolor = Nothing
        Me.txtSTODprocess.Text = "STOD band processing"
        '

    End Sub
    Private Sub CopyTenderFileToStagingArea()

        System.IO.File.Copy(Me.txtFileName_Selected.Text, "\\UK-OL1-FILE-01\group shares\Data\Tender Data Import\" & Me.txtCustomerName_Selected.Text & ".txt", True)

    End Sub


    Private Sub btnCheckHHDataFile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCheckHHDataFile.Click
        ClearFileChecks()

        Dim intCheck As Integer = 0 'IF the intCheck < then the number of checks then dont process the import
        Dim intTotalCheck As Integer = 5 ' 5 Total Checks

        Dim strFilename As String = Me.txtFileName_Selected.Text
        Dim strNewFilename As String = "\\UK-OL1-FILE-01\group shares\Data\Tender Data Import\" & Me.txtCustomerName_Selected.Text & ".txt"
        Dim intTenderID As Integer = Me.lstTenderName.SelectedItem.row.item(0)

        Dim strCheckPath As String = Core.data_select_value("EXEC UML_CMS.dbo.usp_TenderData_CheckDataFile_Path " & intTenderID & ", '" & strFilename & "'")

        If strCheckPath.StartsWith("Error:") Then
            Me.txtLoadStatus_Path.BackColor = Color.Red
            Me.txtLoadStatus_Path.Text = "is the tender file path ok (" & Replace(strCheckPath, "Error: ", "") & ")"
        Else
            Me.txtLoadStatus_Path.BackColor = Color.LimeGreen
            Me.txtLoadStatus_Path.Text = "is the tender file path ok"
            intCheck += 1
        End If
        Me.Refresh()

        Dim strCheckTenderLoaded As String = Core.data_select_value("EXEC UML_CMS.dbo.usp_TenderData_CheckDataFile_TenderLoaded " & intTenderID & ", '" & strFilename & "'")

        If strCheckTenderLoaded.StartsWith("Error:") Then
            Me.txtLoadStatus_TenderLoaded.BackColor = Color.Red
            Me.txtLoadStatus_TenderLoaded.Text = "has the tender already been loaded (" & Replace(strCheckTenderLoaded, "Error: ", "") & ")"
        Else
            Me.txtLoadStatus_TenderLoaded.BackColor = Color.LimeGreen
            Me.txtLoadStatus_TenderLoaded.Text = "has the tender already been loaded"
            intCheck += 1
        End If
        Me.Refresh()


        CopyTenderFileToStagingArea()

        If System.IO.File.Exists(strNewFilename) = False Then
            Me.txtCopyFileToStaging.BackColor = Color.Red
            Me.txtCopyFileToStaging.Text = "copy file to staging area (Error)"
        Else
            Me.txtCopyFileToStaging.BackColor = Color.LimeGreen
            Me.txtCopyFileToStaging.Text = "copy file to staging area"
            intCheck += 1
        End If
        Me.Refresh()

        Dim strSLQ = "EXEC UML_CMS.dbo.usp_TenderData_CheckDataFile_CheckDays " & intTenderID & ", '" & strNewFilename & "'"

        Dim strCheckDays As String = Core.data_select_value(strSLQ)
        'MsgBox(strCheckDays)

        If strCheckDays.StartsWith("Error:") Then
            Me.txtLoadStatus_CheckDays.BackColor = Color.Red
            Me.txtLoadStatus_CheckDays.Text = "are there the correct number of days data (" & Replace(strCheckDays, "Error: ", "") & ")"
        ElseIf strCheckDays = "0" Then
            Me.txtLoadStatus_CheckDays.BackColor = Color.Red
            Me.txtLoadStatus_CheckDays.Text = "are there the correct number of days data (SQL Error)"
        Else
            Me.txtLoadStatus_CheckDays.BackColor = Color.LimeGreen
            Me.txtLoadStatus_CheckDays.Text = "are there the correct number of days data"
            intCheck += 1
        End If
        Me.Refresh()

        Dim strCheckMPAN As String = Core.data_select_value("EXEC UML_CMS.dbo.usp_TenderData_CheckDataFile_CheckMPAN " & intTenderID & ", '" & strNewFilename & "'")
        Dim strDBMPAN As String = ""
        Dim MPAN_Count As Integer = 0

        Dim x As Integer
        ' Loop through all items the ListBox.
        For x = 0 To lstMPANs.Items.Count - 1
            ' Determine if the item is selected.
            If lstMPANs.GetSelected(x) = True Then
                If MPAN_Count = 0 Then
                    strDBMPAN = Me.lstMPANs.SelectedItem.row.item(0)
                Else
                    strDBMPAN += ", " + lstMPANs.GetSelected(x)
                End If

                MPAN_Count += 1
            End If
        Next x

        If strDBMPAN.Contains(strCheckMPAN) = True Then
            Me.txtLoadStatus_MPAN.BackColor = Color.LimeGreen
            Me.txtLoadStatus_MPAN.Text = "does the MPAN match with the file"
            intCheck += 1
        ElseIf strCheckDays = "0" Then
            Me.txtLoadStatus_MPAN.BackColor = Color.Red
            Me.txtLoadStatus_MPAN.Text = "does the MPAN match with the file (SQL Error)"
        Else
            Me.txtLoadStatus_MPAN.BackColor = Color.Red
            Me.txtLoadStatus_MPAN.Text = "does the MPAN match with the file (" & Replace(strCheckMPAN, "Error: ", "") & ") (" & strDBMPAN & ")"
        End If
        Me.Refresh()

        If intCheck = intTotalCheck Then

            Dim strImportData As String = Core.data_select_value("EXEC UML_CMS.dbo.usp_TenderData_CheckDataFile_ImportData " & intTenderID & ", '" & strNewFilename & "'")

            If strImportData = "Success" Then
                Me.txtImportData.BackColor = Color.LimeGreen
                Me.txtImportData.Text = "data imported"
            Else
                Me.txtImportData.BackColor = Color.Red
                Me.txtImportData.Text = "data imported (SQL Error)"
            End If


            Dim strDUOS_TUOS_LAFCharges As String = Core.data_select_value("EXEC UML_CMS.dbo.usp_TenderData_CheckDataFile_DUOS_TUOS_LAFCharges " & intTenderID & ", '" & strNewFilename & "'")

            If strDUOS_TUOS_LAFCharges = "Success" Then
                Me.txtSTODprocess.BackColor = Color.LimeGreen
                Me.txtSTODprocess.Text = "STOD band processing"
            Else
                Me.txtSTODprocess.BackColor = Color.Red
                Me.txtSTODprocess.Text = "STOD band processing (SQL Error)"
            End If
        Else
            Me.txtImportData.BackColor = Color.Yellow
            Me.txtImportData.Text = "data imported (skipped - correct failures first)"

            Me.txtSTODprocess.BackColor = Color.Yellow
            Me.txtSTODprocess.Text = "STOD band processing (skipped - correct failures first)"
        End If
        Me.Refresh()



        If System.IO.File.Exists(strNewFilename) Then System.IO.File.Delete(strNewFilename)

        If System.IO.File.Exists(strNewFilename) Then
            Me.txtCleaningProcess.BackColor = Color.Red
            Me.txtCleaningProcess.Text = "cleaning process (Temp File Still Exists)"
        Else
            Me.txtCleaningProcess.BackColor = Color.LimeGreen
            Me.txtCleaningProcess.Text = "cleaning process"
        End If

    End Sub

    Private Sub btnExitApplication_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExitApplication.Click
        Application.Exit()
    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        Me.Text = "Tender Data Loading Application | " & Now & " | " & Replace(System.Environment.UserName, "UML\", "") & " | " & VersionNumber()
    End Sub

    Function VersionNumber() As String
        Dim VersionNo As System.Version =
        System.Reflection.Assembly.GetExecutingAssembly().GetName().Version

        Return VersionNo.Major.ToString & "." & _
        VersionNo.Minor.ToString & "." & _
        VersionNo.Build.ToString & "." & _
        VersionNo.Revision.ToString
    End Function


End Class
